# Adding a module dependency to fpubase8-deploy

1. `composer require drupal/modulename`
2. Add `modulename` as a dependency in the fpubase8_deploy.info.yml
3. If necessary, add `hook_update_8###` in fpubase8_deploy.install file
    * Copy the example hook_update function
    * Increment the update number in the function name
    * If configuration/settings need to be persisted for new websites, add necessary code to `fpubase8_deploy_install()` or export config to `config/install` or `config/optional`.
4. Commit changes to a new feature branch and create a pull request.

# Updating a site using fpubase8-deploy

**Note:** Test updates thoroughly on a local development environment prior to updating a production website.

In your site project directory, run the following:

* `composer update fpu/fpubase8-deploy --with-dependencies`
* `drush @sitealias.local updb`
* Verify fpubase8-deploy updates were successfully deployed.
* Re-run first two steps on production server after putting site into maintenance mode.